package core;

/*
        Developed by Beny Fishev for Intuit testing Java Juniors
*/


import service.H2OFileParser;
import utils.PrintUtil;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    private static String FILE_PATH = "src/main/resources/html/";

    public static void main(String[] args) {

        H2OFileParser mp = new H2OFileParser();
        PrintUtil pUtil = new PrintUtil();

        ArrayList<String> urlList = new ArrayList<>();
        if (args.length > 0){
            for (String str : args){
                urlList.add(FILE_PATH + str);
            }
            try {
                pUtil.printToConsole(mp.getAccounts(urlList));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(" Please, type full path to file or press Enter to parse default AccountInfo.html ");
            Scanner in = new Scanner(System.in);
            String str = in.nextLine();
            if (str.isEmpty()){
                str = FILE_PATH + "AccountInfo.html";
            }
            try {
                pUtil.printToConsole(mp.getAccount(str));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
