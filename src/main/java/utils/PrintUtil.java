package utils;

import entity.Account;
import entity.Bill;
import entity.Payment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class PrintUtil {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    private static final SimpleDateFormat MOUNTH_FORMAT = new SimpleDateFormat("MM/yyyy");

    private static final String APPLIED = "Applied";
    private static final String TODAY = "1/1/2016";

    public void printToConsole(Account acc) throws ParseException {
        String res =
                "Account nunmer: " + acc.getAccountNumber() + "\n" +
                        "Account name: " + acc.getAccountName() + "\n";
        if (acc.getAccountStatus())
            res += "Satus: Active\n" + "$" + acc.getAccountBalance() + "\n";
        res += getBill(acc, "11", "2015") + "\n" +
                "Last week payments:\n" + getPaymentsLastWeek(acc) +
                "Payments sum in November, 2015:\t" + "$" + getPaymentsSumInMounth(acc, "11","2015");

        System.out.println(res);
    }

    private String getBill(Account acc, String mounth, String year) {
        String tmp = mounth + "/" + year;
        for (Bill bill : acc.getBills()) {
            if (MOUNTH_FORMAT.format(bill.getBillDate()).equals(tmp)) {
                return "Bill Amount: $" + bill.getBillAmount() + "\tDue Date: " + DATE_FORMAT.format(bill.getDueDate());
            }
        }
        return "no data";
    }

    private String getPaymentsLastWeek(Account acc) throws ParseException {
        String res = "";

        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(DATE_FORMAT.parse(TODAY).getTime());
        cal.add(Calendar.WEEK_OF_MONTH, -1);

        for (Payment p : acc.getPayments()) {
            if (p.getPaymentDate().compareTo(cal.getTime()) >= 0) {
                res += "Payment amount: $" + p.getPaymentAmount() + "\t" + DATE_FORMAT.format(p.getPaymentDate()) + "\n";
            }
        }
        return res;
    }

    private double getPaymentsSumInMounth(Account acc, String mounth, String year) {
        double res = 0;
        String tmp = mounth + "/" + year;
        for (Payment p : acc.getPayments()) {
            if (MOUNTH_FORMAT.format(p.getPaymentDate()).equals(tmp) && p.getPaymentStatus().equals(APPLIED)) {
                res += p.getPaymentAmount();
            }
        }
        return res;
    }

    public void printToConsole(List<Account> accounts) throws ParseException {
        for (Account acc : accounts) {
            printToConsole(acc);
        }
    }
}
