package service;

import entity.Account;
import entity.Bill;
import entity.Payment;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class H2OFileParser {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");

    private static final int PAYM_DATE_COL = 0;
    private static final int PAYM_AMOUNT_COL = 1;
    private static final int PAYM_TYPE_COL = 2;
    private static final int PAYM_STAT_COL = 3;

    private static final int BILL_DATE_COL = 0;
    private static final int BILL_AMOUNT_COL = 1;
    private static final int DUE_DATE_COL = 2;

    private static final String ACC_PAY_HTML = "src/main/resources/html/";

    private static final String ACCOUNT_NUMBER = "#accountNumber";
    private static final String ACCOUNT_NAME = "#AccountName";
    private static final String ACCOUNT_ADR = "#Address";
    private static final String ACCOUNT_PHONE = "#medLbl";
    private static final String ACCOUNT_ACTIVE = "ACTIVE";
    private static final String ACCOUNT_BALANCE = "#AccountBalance";
    private static final String ACCOUNT_ACT_CLASS = ".springAlignRightLbl";

    public List<Account> getAccounts(List<String> urlList) throws IOException, ParseException {
        ArrayList<Account> accList = new ArrayList<>();
        for (String str : urlList) {
            accList.add(getAccount(str));
        }
        return accList;
    }

    public Account getAccount(String url) throws IOException, ParseException {
        Document doc = getFile(url);

        Element hist = doc.getElementsByAttributeValue("title", "Payment History").first();
        String payHistLink = hist.attr("href");

        /*
        If it was server request, I'll use by
            String payHistLinkAbs = hist.absUrl("href");
            getLink(payHistLinkAbs)
         */

        Account mAcc = new Account();

        mAcc.setAccountNumber(doc.select(ACCOUNT_NUMBER).first().text());
        mAcc.setAccountName(doc.select(ACCOUNT_NAME).first().text());
        mAcc.setAccountAddress(doc.select(ACCOUNT_ADR).first().text());
        mAcc.setAccountPhone(String.valueOf(doc.getElementsContainingOwnText("Phone Number:").first().nextSibling()));
        mAcc.setAccountStatus(doc.select(ACCOUNT_ACT_CLASS).first().text().substring(0,6).compareToIgnoreCase(ACCOUNT_ACTIVE) == 0 ? true : false);
        if (mAcc.getAccountStatus()) {
            mAcc.setAccountBalance(Double.parseDouble(doc.select(ACCOUNT_BALANCE).first().text().substring(1)));
        }

        doc = getFile(ACC_PAY_HTML + payHistLink.split("\\?")[0]);
        mAcc.setBills(fillBillRecords(doc));
        mAcc.setPayments(fillPaymentRecords(doc));

        return mAcc;
    }

    private ArrayList<Bill> fillBillRecords(Document doc) throws ParseException {
        ArrayList<Bill> aL = new ArrayList<>();

        Element table = doc.select("tbody").first();
        Elements rows = table.select("tr");

        ArrayList<String> tmp = new ArrayList();
        for (Element rowElem : rows) {
            Elements cols = rowElem.select("td");
            for (Element e : cols) {
                tmp.add(e.text().toString());
            }
            aL.add(this.getBillRecord(tmp));
            tmp.clear();
        }
        return aL;
    }

    private Bill getBillRecord(List<String> list) throws ParseException {
        Bill br = new Bill();
        if (!list.isEmpty()) {
            br.setBillDate(DATE_FORMAT.parse(list.get(BILL_DATE_COL)));
            br.setDueDate(DATE_FORMAT.parse(list.get(DUE_DATE_COL)));
            br.setBillAmount(Double.valueOf(list.get(BILL_AMOUNT_COL).substring(1)));
        }
        return br;
    }

    private ArrayList<Payment> fillPaymentRecords(Document doc) throws IOException, ParseException {
        ArrayList<Payment> aL = new ArrayList<>();

        Element table = doc.select("tbody").last();
        Elements rows = table.select("tr");

        ArrayList<String> tmp = new ArrayList();
        for (Element rowElem : rows) {
            Elements cols = rowElem.select("td");
            for (Element e : cols) {
                tmp.add(e.text().toString());
            }
            aL.add(this.getPaymentRecord(tmp));
            tmp.clear();
        }
        return aL;
    }

    private Payment getPaymentRecord(List<String> list) throws ParseException {
        Payment pr = new Payment();

        pr.setPaymentDate(DATE_FORMAT.parse(list.get(PAYM_DATE_COL)));
        pr.setPaymentAmount(Double.valueOf(list.get(PAYM_AMOUNT_COL).substring(1)));
        pr.setPaymentType(list.get(PAYM_TYPE_COL));
        pr.setPaymentStatus(list.get(PAYM_STAT_COL));

        return pr;
    }

    private Document getFile(String path) throws IOException {
        String strHTML = "";
        File file = new File(path);
        strHTML = FileUtils.readFileToString(file);

        return Jsoup.parse(strHTML);
    }

    private Document getLink(String path) throws IOException {
        return Jsoup.connect(path).get();
    }

}
